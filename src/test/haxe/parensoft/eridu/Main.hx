package parensoft.eridu;

import parensoft.eridu.io.InputReader;
import parensoft.eridu.io.OutputWriter;

import scopes.ScopeSyntax;

/**
 * ...
 * @author Parensoft.NET
 */
class Main implements ScopeSyntax {

  public static function main(): Void {
    
    if (Sys.args()[0] == "--list") {
      for (charset in CharsetRegistry.list())
        Sys.println(charset);
      Sys.exit(0);
    }
    
    var rd = new InputReader(Sys.stdin(), CharsetRegistry.byName(Sys.args()[0]));
    @closes var wr = new OutputWriter(Sys.stdout(), CharsetRegistry.byName(Sys.args()[1]));
    
    wr.writeReader(rd);
    
  }
  
}