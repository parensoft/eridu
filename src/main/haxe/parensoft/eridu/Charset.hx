/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu;
import haxe.io.Bytes;
import haxe.io.Input;
import haxe.io.Output;

/**
 * @author Parensoft.NET
 */

interface Charset {
    
  /**
   * Encode a string (platform dependent) as Bytes of this Charset
   */
  public function encode(aString: String): Bytes;
  
  /**
   * Decode Bytes to a (platform dependent) String
   */
  public function decode(aBytes: Bytes): String;

  /**
   * Check whether this Charset allows multibyte characters.
   * 
   */
  public function isMultibyte(): Bool;
  
  /**
   * Encode sequence of unicode code points to bytes of this Charset
   * 
   */
  public function encodeCodePoints(aUniData: Iterable<Int>): Bytes;

  /**
   * Decode bytes into an array of unicode code points
   * 
   */
  public function decodeBytes(aBytes: Bytes): Array<Int>;
  
  /**
   * Reads a unicode character from a stream
   * 
   */
  public function readChar(anInput: Input): Int;
  
  /**
   * Writes a unicode character to a stream
   * 
   */
  public function writeChar(anOutput: Output, aChar: Int): Void;
  
  
  /**
   * Checks whether a character can be encoded in this encoding
   * 
   */
  public function isValidChar(aChar: Int): Bool;
  
}