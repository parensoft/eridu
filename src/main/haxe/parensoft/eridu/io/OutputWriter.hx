/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu.io;
import haxe.io.Eof;
import haxe.io.Output;
import parensoft.eridu.Charset;
import parensoft.eridu.io.Reader;
import scopes.ScopeSyntax;
import unifill.InternalEncodingIter;
import unifill.InternalEncoding;

/**
 * ...
 * @author Parensoft.NET
 */
class OutputWriter implements Writer implements ScopeSyntax {
  
  private var output: Output;
  private var charset: Charset;

  public function new(anOutput: Output, aCharset: Charset) {
    output = anOutput;
    charset = aCharset;
  }
  
  /* INTERFACE parensoft.eridu.io.Writer */
  
  public function close():Void {
    output.close();
  }
  
  public function flush():Void {
    output.flush();
  }
  
  public function write(aString:String):Void {
    for (cp in new CodePointIter(aString)) {
      writeChar(cp);
    }
  }
  
  public function writeChar(aChar:Int):Void {
    charset.writeChar(output, aChar);
  }
  
  public function writeReader(aReader:Reader):Void {
    @quell(Eof) while (true) writeChar(aReader.readChar());
  }
  
}

