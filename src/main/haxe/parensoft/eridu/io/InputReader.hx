/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu.io;
import haxe.io.Eof;
import haxe.io.Input;
import parensoft.eridu.Charset;
import scopes.ScopeSyntax;
import unifill.InternalEncoding;

/**
 * ...
 * @author Parensoft.NET
 */
class InputReader implements Reader implements ScopeSyntax {

  private var charset: Charset;
  private var input: Input;
  
  private var buffer: Array<Int> = [];
  
  public function new(anInput: Input, aCharset: Charset) {
    input = anInput;
    charset = aCharset;
  }
  
  /* INTERFACE parensoft.eridu.io.Reader */
  
  public function close():Void {
    input.close();
  }
  
  public function read(nChars:Int):String {
    var chars = [];
    var count = 0;
    
    @quell(Eof) while (count++ < nChars) chars.push(readChar());
    
    return InternalEncoding.fromCodePoints(chars);
    
  }
  
  public function readAll():String {
    var buf = new StringBuf();
    
    @quell(Eof) while (true) buf.add(InternalEncoding.fromCodePoint(readChar()));
    
    return buf.toString();
  }
  
  public function readChar():Int {
    if (buffer.length > 0) return buffer.shift();
    return charset.readChar(input);
  }
  
  public function readFull(nChars:Int):String {
    var chars = [];
    var count = 0;
    
    @quell(Eof) while (count++ < nChars) chars.push(readChar());
    if (chars.length < nChars) throw new Eof();
    
    return InternalEncoding.fromCodePoints(chars);
    
  }
  
  public function readLine():String {
    var chars = [];
    
    while (true) {
      
      var char = readChar();
      
      if (char == "\n".code) {
        break;
      }
      else if (char == "\r".code) {
        var next = readChar();
        if (next != "\n".code) buffer.push(next);
        
        break;
      }
      else {
        chars.push(char);
      }
      
    }
    
    return InternalEncoding.fromCodePoints(chars);
    
  }
  
  public function readUntil(end:Int):String {
    var chars = [];
    var char = -1;
    
    while (char != end) chars.push(char = readChar());
    
    return InternalEncoding.fromCodePoints(chars);
  }
    

  
}