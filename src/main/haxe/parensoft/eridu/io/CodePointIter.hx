/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu.io;

import unifill.InternalEncodingIter;
import unifill.InternalEncoding;

// Copied from Unifill since current haxelib version does not have this class.

/**
 * ...
 */
class CodePointIter {
  var s : String;
  var itr : InternalEncodingIter;
  
  public inline function new(s : String) {
    this.s = s;
    this.itr = new InternalEncodingIter(s, 0, s.length);
  }

  public inline function hasNext() : Bool {
    return itr.hasNext();
  }

  public inline function next() {
    return InternalEncoding.codePointAt(s, itr.next());
    
  }
  
  public function iterator() {
    return this;
  }

}