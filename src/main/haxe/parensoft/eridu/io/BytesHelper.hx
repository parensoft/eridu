/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu.io;

import haxe.io.BytesInput;
import haxe.io.BytesOutput;
import haxe.io.Eof;
import haxe.io.Output;
import haxe.io.Input;
import haxe.io.Bytes;
import scopes.ScopeSyntax;

/**
 * A base class for charsets. 
 * 
 * @author Parensoft.NET
 */
class BytesHelper implements ScopeSyntax {
  
  public static function encode(aUniData:Iterable<Int>, aCharset: Charset):Bytes {
    
    var stream = new BytesOutput();
    
    for (codePoint in aUniData) {
      aCharset.writeChar(stream, codePoint);
    }
    
    return stream.getBytes();
    
  }
  
  public static function decode(aBytes:Bytes, aCharset: Charset):Array<Int> {
    
    @closes var stream = new BytesInput(aBytes);
    var ret = [];
    
    @quell(Eof) while (true) ret.push(aCharset.readChar(stream));

    return ret;
    
  }
  
}