/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu.io;

import haxe.io.Eof;
import unifill.InternalEncoding;

/**
 * ...
 * @author Parensoft.NET
 */
@:forward(close, read, readAll, readChar, readFull, readLine, readUntil, atEof)
abstract BufferingReader(BufferingReaderImpl) to Reader {
  
  public function new(aReader: Reader) {
    if (Std.is(aReader, BufferingReaderImpl)) {
      this = cast aReader;
    }
    else {
      this = new BufferingReaderImpl(aReader);
    }
  }
  
  @:noCompletion
  @:from
  public static function fromReader(aReader: Reader) {
    return new BufferingReader(aReader);
  }
  
  @:noCompletion
  @:arrayAccess
  public function arrayRead(idx: Int): Int {
    return this.peekAt(idx);
  }
  
  public var maxReadAhead(get, set): Int;
  
  private function get_maxReadAhead() return this.maxReadAhead;
  
  private function set_maxReadAhead(aCap: Int) return this.maxReadAhead = aCap;
  
}
 
 
private class BufferingReaderImpl implements Reader {

  private var reader: Reader;
  private var buffer: Array<Int> = [];
  
  public var maxReadAhead(default, default) = 128;
  
  private var closed = false;
  
  public function new(aReader: Reader) {
    reader = aReader;
  }
  
  public function close() {
    closed = true;
    reader.close();
  }
  
  public function readChar(): Int {
    checkState();
    
    if (buffer.length == 0) readAhead();

    if (buffer.length == 0) throw new Eof();
    
    return buffer.shift();
    
  }
  
  public function read(nChars: Int) {
    checkState();
    
    
    if (nChars <= buffer.length) {
      return takeChars(nChars);
    }
    else if (nChars <= maxReadAhead) {
      readAhead();
      
      var realToRead: Int = min(nChars, buffer.length);
      
      return takeChars(realToRead);
    }
    else {
      var readMore = nChars;
      var buf = new StringBuf();
      
      while (readMore > 0) {
        readAhead();
        
        var reallyRead = buffer.length;

        if (buffer.length == 0) break;
        
        buf.add(takeChars(min(reallyRead, readMore)));

        if (reallyRead < maxReadAhead) break;
        
        readMore -= reallyRead;
      }
      
      return buf.toString();
      
    }
    
  }
  
  public function readAll() {
    checkState();

    var buf = new StringBuf();
    
    buf.add(takeChars(buffer.length));
    
    buf.add(reader.readAll());
    
    return buf.toString();
    
  }
  
  public function readFull(nChars: Int): String {
    checkState();

    var reading = read(nChars);
    
    if (InternalEncoding.codePointCount(reading, 0, reading.length) < nChars) throw new Eof();
    
    return reading;
  }
  
  public function readLine(): String {
    checkState();

    var buf = new StringBuf();
    
    var nidx = buffer.indexOf("\n".code);
    var ridx = buffer.indexOf("\r".code);
    
    if (nidx == -1 && ridx == -1) {
      buf.add(takeChars(buffer.length));
      buf.add(reader.readLine());
    }
    else if (nidx >= 0 && (ridx == -1 || ridx > nidx)) {
      buf.add(takeChars(nidx));
      buffer.shift(); // remove \n
    }
    else if (ridx == nidx - 1) { // \r\n, nidx==0, ridx==-1 impossible now
      buf.add(takeChars(ridx));
      buffer.splice(0, 2); // \r\n
    }
    else if (ridx == buffer.length - 1) {
      buf.add(takeChars(ridx));
      buffer.shift(); // buffer now empty but \n may be next
      readAhead();
      if (buffer[0] == "\n".code) buffer.shift(); // take \n if found
    }
    else { // \r found, not last or followed by \n
      buf.add(takeChars(ridx));
      buffer.shift();
    }
    
    return buf.toString();
  }
  
  public function readUntil(anEnd: Int) {
    checkState();

    var buf = new StringBuf();
    
    var idx = buffer.indexOf(anEnd);
    
    if (idx >= 0) {
      buf.add(takeChars(idx));
      buffer.shift();
    }
    else {
      buf.add(takeChars(buffer.length));
      buf.add(reader.readUntil(anEnd));
    }
    
    return buf.toString();
  }
  
  public function peekAt(aPos: Int): Int {
    checkState();

    if (aPos > maxReadAhead) {
      throw new IoException('attempt to peek past buffer size: $aPos > $maxReadAhead');
    }
    
    if (aPos >= buffer.length) {
      readAhead();
    }
    
    if (aPos >= buffer.length) {
      throw new ReadPastEndException();
    }
    
    return buffer[aPos];
    
  }

  public function atEof(): Bool {
    checkState();

    if (buffer.length == 0) readAhead();

    return buffer.length == 0;
  }
  
  private inline function takeChars(nChars: Int) {
    return InternalEncoding.fromCodePoints(buffer.splice(0, nChars));
  }
  
  private function readAhead() {
    var toRead = maxReadAhead - buffer.length;
    
    var whatRead = new CodePointIter(reader.read(toRead));
    
    for (cp in whatRead) buffer.push(cp);
    
  }
  
  private inline function checkState(): Void {
    if (closed) throw new IoException("reader already closed");
  }
  
  private inline static function min(a: Int, b: Int) {
    return a > b ? b : a;
  }
  
}
