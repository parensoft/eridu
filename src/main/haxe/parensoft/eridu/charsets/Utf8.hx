/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu.charsets;
import haxe.io.Eof;
import haxe.io.Output;
import haxe.io.Input;
import unifill.Unicode;

import haxe.io.Bytes;
import parensoft.eridu.Charset;
import parensoft.eridu.io.BytesHelper;

import unifill.Utf8 in UniUtf;

/**
 * UTF-8
 */
class Utf8 extends BaseCharset implements Charset {
  
  public function new() {
    super();
  }
  
  /* INTERFACE parensoft.eridu.Charset */
  
  public function isMultibyte():Bool {
    return true;
  }
  
  public override function encodeCodePoints(aUniData:Iterable<Int>):Bytes {
    return BytesHelper.encode(aUniData, this);
  }
  
  public override function decodeBytes(aBytes:Bytes):Array<Int> {
    return BytesHelper.decode(aBytes, this);
  }
  
  public function readChar(anInput:Input):Int {
    
    while (true) {
      var byte = anInput.readByte();
      
      if (byte < 0x80) {
        return byte;
      }
      
      if (byte < 0xC0) {
        throw 'Invalid UTF-8 byte: ${byte}';
      }
      
      try if (byte < 0xE0) {
        var buf = Bytes.alloc(2);
        
        buf.set(0, byte);
        anInput.readFullBytes(buf, 1, 1);
        
        return UniUtf.fromBytes(buf).codePointAt(0);
      }
      else if (byte < 0xF0) {
        var buf = Bytes.alloc(3);
        
        buf.set(0, byte);
        anInput.readFullBytes(buf, 1, 2);
        
        var cp = UniUtf.fromBytes(buf).codePointAt(0);
        
        if (cp == 0xFEFF) continue;
        else return cp;
      }
      else if (byte < 0xF8) {
        var buf = Bytes.alloc(4);
        
        buf.set(0, byte);
        anInput.readFullBytes(buf, 1, 3);

        return UniUtf.fromBytes(buf).codePointAt(0);
      }
      else {
        throw 'Invalid UTF-8 byte: ${byte}';
      }
      catch (eof: Eof) {
        throw 'premature end of file';
      }
    }

  }
  
  public function writeChar(anOutput:Output, aChar:Int):Void {
    
    anOutput.write(UniUtf.fromCodePoint(aChar).toBytes());
    
  }
  
  public function isValidChar(aChar:Int):Bool {
    return Unicode.isScalar(aChar);
  }
  
}
