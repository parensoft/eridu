/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu.charsets;

import haxe.io.Bytes;
import haxe.io.BytesBuffer;
import haxe.io.Input;
import haxe.io.Output;

/**
 * ...
 * @author Parensoft.NET
 */
class SingleByteCharset extends BaseCharset {

  private var offset: Int;
  private var maxPoint: Int;
  
  private var decodes(get, never): Array<Int>;
  private var encodes(get, never): Array<Int>;
  private var encodeIndices(get, never): Array<Int>;
  
  /**
   * ISO-8859 charset base 
   * 
   * @param anOffset least byte that decodes different than in ISO-8859-1
   * @param aMaxPoint highest codepoint in this charset
   */
  public function new(anOffset: Int, aMaxPoint:Int) {
    super();
    
    offset = anOffset;
    maxPoint = aMaxPoint;
  }
  
  
  public function isMultibyte():Bool {
    return false;
  }
  
  public override function encodeCodePoints(aUniData:Iterable<Int>):Bytes {
    var buf = new BytesBuffer();
    
    for (chr in aUniData) buf.addByte(charToByte(chr));
    
    return buf.getBytes();
  }
  
  public override function decodeBytes(aBytes:Bytes):Array<Int> {
    var idx = 0;
    var data = aBytes.getData();
    var ret = [];
    
    while (idx < aBytes.length) {
      ret.push(byteToChar(Bytes.fastGet(data, idx)));
    }
    
    return ret;
  }
  
  public function readChar(anInput:Input):Int {
    return byteToChar(anInput.readByte());
  }
  
  public function writeChar(anOutput:Output, aChar:Int):Void {
    anOutput.writeByte(charToByte(aChar));
  }
  
  public function isValidChar(aChar:Int):Bool {
    try {
      return charToByte(aChar) > 0;
    }
    catch (ex: String) return false;
  }
  
  function byteToChar(aByte: Int): Int {
    if (aByte >= 0 && aByte < offset) {
      return aByte;
    }
    else if (aByte >= offset && aByte < 256) {
      return decodes[aByte - offset];
    }
    else {
      throw 'invalid byte $aByte';
    }
    
  }
  
  function charToByte(aChar: Int): Int {
    if (aChar > maxPoint) throw 'invalid char $aChar';
    
    if (aChar <= offset) return aChar;
    
    var idx = aChar >> 8;
    idx = encodeIndices[idx];
    if (idx < 0) throw 'invalid char $aChar';
    idx <<= 8;
    idx += (aChar & 0xFF);
    
    if (encodes[idx] != 0) return encodes[idx];
    
    throw 'invalid char $aChar';
    
  }

  private function get_decodes(): Array<Int> {
    throw 'implement!';
  }
  
  private function get_encodes(): Array<Int> {
    throw 'implement!';
  }
  
  private function get_encodeIndices(): Array<Int> {
    throw 'implement!';
  }


}