/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu.charsets;
import haxe.io.Output;
import haxe.io.Input;
import unifill.Unicode;

import haxe.io.Bytes;
import parensoft.eridu.Charset;
import parensoft.eridu.io.BytesHelper;

/**
 * WARNING This charset is stateful. A BOM encountered in the stream sets the byte order
 * for the chars to come until another BOM is encountered. Thus, for every new stream a new instance should
 * be created/obtained.
 * 
 * A new instance is created with byte order set to Big Endian. It may be explicitly set to Little Endian
 * by setting the property `bigEndian`.
 * 
 * There are subclasses Utf16BE and Utf16LE which do not allow setting this property (throw an exception). These
 * will throw an exception as well if a BOM enforcing other endianness is encountered.
 * 
 */
class Utf16 extends BaseCharset implements Charset{

  public var bigEndian(default, set): Bool = true;
  
  public function new() {
    super();
  }
  
  /* INTERFACE parensoft.eridu.Charset */
  
  public function isMultibyte():Bool {
    return true;
  }
  
  public override function encodeCodePoints(aUniData:Iterable<Int>):Bytes {
    return BytesHelper.encode(aUniData, this);
  }
  
  public override function decodeBytes(aBytes:Bytes):Array<Int> {
    return BytesHelper.decode(aBytes, this);
  }
  
  public function readChar(anInput:Input):Int {
    var code: Int = -1;
    
    while (true) {
      code = anInput.readByte() << 8 | anInput.readByte();
      
      if (code == 0xFFFE) bigEndian = false;
      else if (code == 0xFEFF) bigEndian = true;
      else break;

    }
    
    if (!bigEndian) code = ((code & 0xFF00) >> 8) | ((code & 0xFF) << 8);
    
    if (code >= 0xD800 && code <= 0xDFFF) {
      if (code >= 0xDC00) throw 'Not a low surrogate: $code';
      
      var code2 = anInput.readByte() << 8 | anInput.readByte();
      if (!bigEndian) code2 = ((code2 & 0xFF00) >> 8) | ((code2 & 0xFF) << 8);
      
      if (code2 < 0xDC00) throw 'Not a high surrogate: $code2';
      
      return Unicode.decodeSurrogate(code, code2);
    }
    else {
      return code;
    }
    
  }
  
  public function writeChar(anOutput:Output, aChar:Int):Void {
    if (!isValidChar(aChar)) throw 'invalid unicode char: $aChar';
    if (aChar <= 0xFFFF) {
      writePair(anOutput, aChar);
    }
    else {
      writePair(anOutput, Unicode.encodeHighSurrogate(aChar));
      writePair(anOutput, Unicode.encodeLowSurrogate(aChar));
    }
  }
  
  public function isValidChar(aChar:Int):Bool {
    return Unicode.isScalar(aChar);
  }
  
  private function set_bigEndian(newEndian: Bool): Bool {
    if ((newEndian: Null<Bool>) == null) throw 'null endian set attempt';
    
    return bigEndian = newEndian;
  }
  
  private function writePair(anOutput: Output, aPair: Int): Void {
    if (bigEndian) {
      anOutput.writeByte((aPair & 0xFF00) >> 8);
      anOutput.writeByte(aPair & 0xFF);
    }
    else {
      anOutput.writeByte(aPair & 0xFF);
      anOutput.writeByte((aPair & 0xFF00) >> 8);
    }
  }
}