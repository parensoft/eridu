/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.eridu;
import parensoft.eridu.charsets.*;

/**
 * ...
 * @author Parensoft.NET
 */
class CharsetRegistry {

  private static var knownCharsets: Map<String, Class<Charset>> = [
    "UTF-8" => Utf8,
    "UTF-16" => Utf16,
    "UTF-16LE" => Utf16LE,
    "UTF-16BE" => Utf16BE,
    "ISO-8859-1" => Latin1,
    "ISO-8859-2" => Latin2,
    "KOI8-R" => Koi8R,
    "US-ASCII" => UsAscii,
    "ISO-8859-4" => Latin4,
    "ISO-8859-5" => IsoCyrillic,
    "ISO-8859-9" => Latin5,
    "ISO-8859-13" => Latin7,
    "ISO-8859-15" => Latin9,
	"CP1252" => Cp1252
  ];
  
  public static function byName(aName: String): Charset {
    if (knownCharsets.exists(aName)) {
      return Type.createInstance(knownCharsets[aName], []);
    }
    throw 'Unknown charset: $aName';
  }
  
  public static function list(): Iterator<String> {
    return knownCharsets.keys();
  }
  
  
  
}