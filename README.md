# Library for charset conversion and charset-aware I/O

This library adds support for national character set conversion as well as for character set aware input and output.

Character sets are implementation of the ```Charset``` interface, whose most important methods are:

```
function encode(aString: String): Bytes
function decode(aBytes: Bytes): String
```

They both rely on the [unifill](http://lib.haxe.org/p/unifill) library to deal with native encodings (as do most other methods and UTF conversions).

The charset implementations may be either directly instantiated (e.g. ```new Latin1()```) or obtained by their name from registry (e.g. ```CharsetRegistry.byName("ISO-8859-1")```. Available charset names may be obtained with ```CharsetRegistry.list()```. ATM no method to add a charset to registry is provided.

To do charset-aware I/O there are two interfaces, ```Reader``` and ```Writer``` which provide functionality similar to ```Input``` and ```Output``` from the Haxe standard library.

Basic implementations are provided: ```InputReader``` and ```OutputWriter``` designed to wrap Haxe's ```Input``` and ```Output``` providing character set conversion. E.g. this little program converts stdin from the encoding in the first argument to the one in the second and writes to stdout (this is how I test this library):

```
#!haxe

class Main {

  public static function main(): Void {
    
    var rd = new InputReader(Sys.stdin(), CharsetRegistry.byName(Sys.args()[0]));
    var wr = new OutputWriter(Sys.stdout(), CharsetRegistry.byName(Sys.args()[1]));
    
    wr.writeReader(rd);

  }
  
}
```

### Known bugs/limitations
* Only UTF-8, UTF-16(BE, LE), US-ASCII, ISO-8859: 1, 2, 4, 5, 9, 13, 15, KOI8-R supported

### Version history:
2015.02.11 0.2.0 fix readLine; add charsets: KOI8-R, ISO-8859: 4, 5, 9, 13, 15
2015.01.21 0.1.0 First released

### Roadmap:
* Add remaining ISO-8859 charsets
* Add other popular charsets
